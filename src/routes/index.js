import express from "express";
import authRouter from "./auth";
import restaurantRouter from "./restaurant";
import orderRouter from "./order";

const router = express.Router();
router.use("/auth", authRouter);
router.use("/restaurant", restaurantRouter);
router.use("/order", orderRouter);

export default router;
