import express from 'express';
import { register, login, addAddress } from '../controllers/user';

const router = express.Router();
router.post('/register',  register);
router.get('/login',  login);
router.put('/addAddress',  addAddress);

export default router;
