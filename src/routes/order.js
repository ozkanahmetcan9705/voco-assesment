import express from 'express';
import { placeOrder, getOrderDetails, updateOrderStatus, addCommentAndRating } from '../controllers/order';

const router = express.Router();
router.post('/createOrder',  placeOrder);
router.get('/orderDetail/:orderId', getOrderDetails);
router.put('/updateStatus/:orderId', updateOrderStatus);
router.put('/addCommentAndRating', addCommentAndRating);

export default router;
