import express from 'express';
import { createRestaurant, getRestaurants, createBranch, addMenuToBranch, getTopMaleUsersByAge, addRestaurantType, removeRestaurantType, filterRestaurants, filterByPoint, nearbyRestaurants } from '../controllers/restaurant';

const router = express.Router();
router.post('/addRestaurant',  createRestaurant);
router.get('/getAllRestaurants',  getRestaurants);
router.put('/addBranch',  createBranch);
router.put('/addMenuItem',  addMenuToBranch);
router.patch('/getMaleComments',  getTopMaleUsersByAge);
router.put('/addRestaurantType',  addRestaurantType);
router.delete('/removeRestaurantType',  removeRestaurantType);
router.get('/filter',  filterRestaurants);
router.get('/getRestaurantsByPoint',  filterByPoint);
router.get('/nearbyRestaurants',  nearbyRestaurants);

export default router;
