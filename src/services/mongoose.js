import config from '../config';
import mongoose from 'mongoose';

mongoose.connection.on('connected', () => {
    console.log('MongoDB is connected');
});

mongoose.connection.on('error', (err) => {
    console.log(`Could not connect to MongoDB because of ${err}`);
    process.exit(-1);
});

if (config.env === 'dev') {
    mongoose.set('debug', false);
}

const mongoURI = (config.env === 'prod' || config.env === 'dev') ? config.mongo.uri : config.mongo.testURI;

export default {
    connect: () => mongoose.connect("mongodb://127.0.0.1:27017/voco", {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }),
    disconnect: () => mongoose.disconnect()
};

