import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import helmet from 'helmet';
import bodyParser from 'body-parser';
import config from '../config';
import apiRouter from '../routes/index';
import passport from '../services/passport';
const app = express();


app.use('/uploads', express.static('uploads'));
app.use(bodyParser.json({
    limit: '150mb',
    extended: true
}));
app.use(cors());
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin',"*")
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
app.use(helmet());
if (config.env !== 'test') {
    app.use(morgan('combined'));
}
app.use(passport);
app.use('/api', apiRouter);

export default app;
