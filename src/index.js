import db from './services/mongoose';
import app from './services/express';
import config from './config';
import cors from "cors";

db.connect().then(() => {
    app.use(cors());
    app.listen(config.port, (err) => {
        if (err) {
            console.log(`Error : ${err}`);
            process.exit(-1);
        }
        console.log(`Server ${config.port} portundan çalışıyor `);
    });
});
