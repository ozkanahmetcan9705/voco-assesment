import httpStatus from 'http-status';
import Restaurant from '../models/restaurant';
import User from '../models/user';

export const createRestaurant = async (req, res) => {
    try {
        const restaurant = new Restaurant(req.body);
        const savedRestaurant = await restaurant.save();
        res.status(201).json(savedRestaurant);
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
};

export const getRestaurants = async (req, res) => {
    try {
        const restaurants = await Restaurant.find();
        res.status(200).json(restaurants);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

export const createBranch = async (req, res) => {
    try {
        const restaurantId = req.body.restaurantId; // No destructuring needed
        console.log(restaurantId, "restaurantId");

        const userId = req.body.userId; // No destructuring needed
        console.log(userId, "userId");

        const isOwner = await Restaurant.exists({ _id: restaurantId, owner: userId });
        console.log(isOwner, "isOwner");

        if (!isOwner) {
            return res.status(httpStatus.UNAUTHORIZED).json({ error: 'You are not the owner of this restaurant.' });
        }

        const branch = req.body;
        const restaurant = await Restaurant.findByIdAndUpdate(
            restaurantId,
            { $push: { branches: branch } },
            { new: true }
        );

        res.status(httpStatus.CREATED).json(restaurant);
    } catch (error) {
        res.status(httpStatus.BAD_REQUEST).json({ error: error.message });
    }
};

export const addMenuToBranch = async (req, res) => {
    try {
        const restaurantId = req.body.restaurantId;
        const branchId = req.body.branchId;
        const userId = req.body.userId;

        // Check if the user is the owner of the restaurant or the owner of the branch
        const isOwner = await Restaurant.exists({
            _id: restaurantId,
            $or: [
                { owner: userId }, // Owner of the restaurant
                { 'branches._id': branchId, 'branches.owner': userId }, // Owner of the branch
            ],
        });

        if (!isOwner) {
            return res.status(httpStatus.UNAUTHORIZED).json({ error: 'You are not the owner of this restaurant or branch.' });
        }

        const menuItems = req.body.menu; // Assuming menu is an array of menu items
        const restaurant = await Restaurant.findOneAndUpdate(
            { _id: restaurantId, 'branches._id': branchId },
            { $push: { 'branches.$.menu': { $each: menuItems } } }, // Add the new menu items to the specific branch
            { new: true }
        );

        res.status(httpStatus.CREATED).json(restaurant);
    } catch (error) {
        res.status(httpStatus.BAD_REQUEST).json({ error: error.message });
    }
};

export const getTopMaleUsersByAge = async (req, res) => {
    try {
        const { restaurantId } = req.body;
        const { page = 1, sortOrder = 'asc' } = req.query;

        // Find the restaurant to get the comments and populate the 'comments.user' field
        const restaurant = await Restaurant.findById(restaurantId).populate('comments.user');
        console.log(restaurant, "restaurant")

        if (!restaurant) {
            return res.status(httpStatus.NOT_FOUND).json({ error: 'Restaurant not found.' });
        }

        // Get the comments from the restaurant
        const allComments = restaurant.comments;
        console.log(allComments, "allComments")

        // Filter comments by male users
        const maleComments = await Promise.all(allComments.map(async (comment) => {
            // Fetch the user details from the User collection
            const user = await User.findById(comment.user._id);
            console.log(user, "user");

            // Check if the user is male
            if (user) {
                console.log('User Gender:', user.gender); // Add this line for debugging
                if (user.gender === 'Male') {
                    console.log(comment, "comment");
                    return comment; // Return the entire comment object
                }
            }

            return null;
        }));

// Remove null values from the array
        const filteredMaleComments = maleComments.filter((comment) => comment !== null && comment !== undefined);
        console.log(filteredMaleComments, "filteredMaleComments");

// Check if there are any filtered comments before proceeding
        if (filteredMaleComments.length === 0) {
            return res.status(httpStatus.OK).json([]); // Return an empty array or handle it as needed
        }

// Sort male users by age
        const sortedMaleUsers = filteredMaleComments
            .sort((a, b) => {
                if (sortOrder === 'asc') {
                    return a.user.age - b.user.age;
                } else {
                    return b.user.age - a.user.age;
                }
            });

// Paginate the result
        const pageSize = 20;
        const startIndex = (page - 1) * pageSize;
        const endIndex = startIndex + pageSize;

        const paginatedMaleUsers = sortedMaleUsers.slice(startIndex, endIndex);
        console.log(paginatedMaleUsers, "paginatedMaleUsers")

        res.status(httpStatus.OK).json(paginatedMaleUsers);
    } catch (error) {
        res.status(httpStatus.BAD_REQUEST).json({ error: error.message });
    }
};

export const addRestaurantType = async (req, res) => {
    try {
        const { restaurantId, restaurantType } = req.body;

        // Restoranı bul
        const restaurant = await Restaurant.findById(restaurantId);

        if (!restaurant) {
            return res.status(httpStatus.NOT_FOUND).json({ error: 'Restaurant not found.' });
        }

        // Restoran tipini ekle
        restaurant.restaurantTypes.push(restaurantType);
        await restaurant.save();

        res.status(httpStatus.OK).json(restaurant);
    } catch (error) {
        res.status(httpStatus.BAD_REQUEST).json({ error: error.message });
    }
};

// Restoran tipini çıkarmak için controller
export const removeRestaurantType = async (req, res) => {
    try {
        const { restaurantId, restaurantType } = req.body;

        // Restoranı bul
        const restaurant = await Restaurant.findById(restaurantId);

        if (!restaurant) {
            return res.status(httpStatus.NOT_FOUND).json({ error: 'Restaurant not found.' });
        }

        // Restoran tipini çıkar
        restaurant.restaurantTypes = restaurant.restaurantTypes.filter(type => type !== restaurantType);
        await restaurant.save();

        res.status(httpStatus.OK).json(restaurant);
    } catch (error) {
        res.status(httpStatus.BAD_REQUEST).json({ error: error.message });
    }
};

export const filterRestaurants = async (req, res) => {
    try {

        const filteredRestaurants = await Restaurant.find({
                $or: [
                    { "restaurantTypes": { $in: ["Fast Food", "Ev Yemekleri"] } },
                    { "description": { $regex: /fast/i } }
                ],
                "comments.point": { $gte: 4 }
            },
            {
                "name": 1,
                "restaurantTypes": 1,
                "description": 1,
                "_id": 0
            })

        res.status(httpStatus.OK).json(filteredRestaurants);
    } catch (error) {
        res.status(httpStatus.BAD_REQUEST).json({ error: error.message });
    }
};

export const filterByPoint = async (req, res) => {
    try {
        const { page = 1, pageSize = 10 } = req.query;
        const restaurants = await Restaurant.aggregate([
            {
                $addFields: {
                    averageRating: { $avg: '$comments.point' },
                },
            },
            {
                $sort: { averageRating: -1 },
            },
            {
                $skip: (page - 1) * pageSize,
            },
            {
                $limit: parseInt(pageSize),
            },
        ]);

        res.status(httpStatus.OK).json(restaurants);
    } catch (error) {
        res.status(httpStatus.BAD_REQUEST).json({ error: error.message });
    }
};

export const nearbyRestaurants = async (req, res) => {
    try {
        const { coordinates } = req.body;
        const [latitude, longitude] = coordinates.split(',').map(coord => parseFloat(coord));

        // Lahmacun içeren ve belirtilen koordinatlara en yakın 5 restoranı bul
        const nearbyRestaurants = await Restaurant.find({
            "description": { $regex: /lahmacun/i }, // Açıklamada lahmacun içeren restoranlar
            "branches.location": {
                $near: {
                    $geometry: {
                        type: "Point",
                        coordinates: [longitude, latitude]
                    },
                    $maxDistance: 10000 // Maksimum 10 km uzaklıktaki restoranları getir
                }
            }
        }, {
            "name": 1,
            "restaurantTypes": 1,
            "description": 1,
            "branches.location": 1,
            "_id": 0
        }).limit(5);

        res.status(httpStatus.OK).json(nearbyRestaurants);
    } catch (error) {
        res.status(httpStatus.BAD_REQUEST).json({ error: error.message });
    }
};






