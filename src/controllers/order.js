// order.controller.js
import httpStatus from 'http-status';
import Order from '../models/order';
import Restaurant from '../models/restaurant';

export const placeOrder = async (req, res) => {
    try {
        const { userId, restaurantId, items } = req.body;
        console.log(req.body, "body")
        const totalPrice = await calculateTotalPrice(items, restaurantId);
        console.log(totalPrice, "price")

        const orderItems = await Promise.all(items.map(async (item) => {
            console.log(item, "item")
            const menuItem = await findMenuItemInBranches(restaurantId, item.menuItemId);
            console.log(menuItem, "menuItem")
            if (!menuItem) {
                throw new Error(`Menu item not found for id: ${item.menuItemId}`);
            }

            return {
                menuItem: {
                    name: menuItem.name,
                    price: menuItem.price,
                    content: menuItem.content,
                },
                quantity: item.quantity,
            };
        }));


        const order = new Order({
            user: userId,
            restaurant: restaurantId,
            items: orderItems,
            totalPrice: totalPrice,
            status: 'Pending',
        });

        const savedOrder = await order.save();
        res.status(httpStatus.CREATED).json(savedOrder);
    } catch (error) {
        res.status(httpStatus.BAD_REQUEST).json({ error: error.message });
    }
};
const calculateTotalPrice = async (items, restaurantId) => {
    let totalPrice = 0;

    const restaurant = await Restaurant.findById(restaurantId.toString());

    if (!restaurant) {
        throw new Error(`Restaurant not found for id: ${restaurantId}`);
    }

    for (const item of items) {
        const menuItem = await findMenuItemInBranches(restaurantId, item.menuItemId);

        // Ensure menuItem is not null and has a valid price
        if (menuItem && typeof menuItem.price === 'number') {
            totalPrice += item.quantity * menuItem.price;
        } else {
            throw new Error(`Invalid menuItem or price for menuItemId: ${item.menuItemId}`);
        }
    }

    return totalPrice;
};


// Helper function to find a menu item in the branches based on menuItemId
const findMenuItemInBranches = async (restaurantId, menuItemId) => {
    const restaurant = await Restaurant.findById(restaurantId);

    if (!restaurant) {
        throw new Error(`Restaurant not found for id: ${restaurantId}`);
    }

    if (!Array.isArray(restaurant.branches)) {
        throw new Error(`Invalid branch structure for the restaurant.`);
    }

    for (const branch of restaurant.branches) {
        const menuItem = branch.menu.find((menu) => menu._id.toString() === menuItemId);
        if (menuItem) {
            return menuItem;
        }
    }
    return null;
};





export const getOrderDetails = async (req, res) => {
    try {
        const { orderId } = req.params;

        // Find the order by orderId
        const order = await Order.findById(orderId);

        if (!order) {
            return res.status(httpStatus.NOT_FOUND).json({ error: 'Order not found' });
        }

        // Extract relevant information from the order
        const orderDetails = {
            items: order.items,
            totalPrice: order.totalPrice,
            status: order.status,
        };

        res.status(httpStatus.OK).json(orderDetails);
    } catch (error) {
        res.status(httpStatus.BAD_REQUEST).json({ error: error.message });
    }
};
export const updateOrderStatus = async (req, res) => {
    try {
        const { orderId } = req.params;
        const { newStatus } = req.body;

        // Find the order by orderId
        const order = await Order.findById(orderId);

        if (!order) {
            return res.status(httpStatus.NOT_FOUND).json({ error: 'Order not found' });
        }

        // Update the order status
        order.status = newStatus;
        await order.save();

        res.status(httpStatus.OK).json({ message: 'Order status updated successfully' });
    } catch (error) {
        res.status(httpStatus.BAD_REQUEST).json({ error: error.message });
    }
};

export const addCommentAndRating = async (req, res) => {
    try {
        const { orderId, userId, text, point } = req.body;
        console.log(req.body, "req.body")

        const order = await Order.findById(orderId);
        console.log(order, "order")
        if (!order) {
            return res.status(httpStatus.NOT_FOUND).json({ error: 'Order not found.' });
        }

        // Check if the user has already given a comment and rating for this order
        const existingComment = order.comments.find((comment) => comment.user.toString() === userId);
        console.log(existingComment, "existingComment")
        if (existingComment) {
            return res.status(httpStatus.BAD_REQUEST).json({ error: 'You have already given a comment and rating for this order.' });
        }

        // Add the comment and rating to the order
        order.comments.push({ user: userId, text, point });
        await order.save();

        // Add the comment and rating to the associated restaurant
        const restaurant = await Restaurant.findById(order.restaurant);
        console.log(restaurant, "restaurant")
        if (!restaurant) {
            return res.status(httpStatus.NOT_FOUND).json({ error: 'Associated restaurant not found.' });
        }
        restaurant.comments.push({ user: userId, text, point });
        await restaurant.save();

        res.status(httpStatus.OK).json(order);
    } catch (error) {
        res.status(httpStatus.BAD_REQUEST).json({ error: error.message });
    }
};

