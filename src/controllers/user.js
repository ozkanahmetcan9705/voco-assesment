import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import config from '../config';
import User from '../models/user';
import bcrypt from "bcrypt-nodejs";
export const register = async (req, res) => {
    try {
        const {email, password, username, gender, age} = req.body;
        const checkDuplicateMail = await User.findOne({email : email}).exec();
        if(!checkDuplicateMail){
            if (password.length < 6){
                return res.json({
                    status: httpStatus.NOT_ACCEPTABLE,
                    message: "Şifre 6 karakter uzunluğunda olmalıdır!"
                })
            }
            const user = new User(req.body);
            await user.save();
            return res.json({
                status: httpStatus.OK,
                message: "Hesap oluşturma işlemi başarılı",
                user
            })
        } else {
            return res.json({
                status: httpStatus.CONFLICT,
                message: "Mail başka bir hesapta kullanılıyor"
            })
        }
    } catch (error) {
        return res.json({
            status: httpStatus.INTERNAL_SERVER_ERROR,
            message: `Beklenmedik bir hata oluştu, ${error}`
        })
    }
};

const passwordMatches = async (password, pass) => {
    return bcrypt.compareSync(password, pass);
};

export const login = async (req, res) => {
    try {
        const {email, password } = req.body;
            const user = await User.findOne({ email: req.body.email }).exec();
            let payload;
            if(user){
                payload = { sub: user.id };
                console.log(payload, "payload");
            }
            if (!user)
                return res.json({
                    status: httpStatus.CONFLICT,
                    message: "Kullanıcı bulunamadı!"
                })
            const passwordOK = await passwordMatches(req.body.password, user.password);
            if (!passwordOK)
                return res.json({
                    status: httpStatus.UNAUTHORIZED,
                    message: "Kullanıcı adı ve şifre uyuşmuyor!"
                })
            let token = jwt.sign(payload, config.secret);
            if (payload &&  passwordOK)
                await User.updateOne({_id:user.id},{$set:{token:token}});
            return res.json({
                status: httpStatus.OK,
                message: 'OK',
                token,
                user
            })

    } catch (error) {
        return res.json({
            status: httpStatus.INTERNAL_SERVER_ERROR,
            message: `Beklenmedik bir hata oluştu, ${error}`
        })
    }
};

export const addAddress = async (req, res, next) => {
    try {
        const query = {
            _id: req.body.id,
        };
        const data = {
            $push: {
                address: req.body.address,
            },
        };
        const updatedAddress = await User.findOneAndUpdate(query, data, {
            new: true,
        }).exec();
        res.status(httpStatus.OK).send(updatedAddress);
    } catch (error) {
        next(error);
    }
};


