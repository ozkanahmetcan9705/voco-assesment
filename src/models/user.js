import { Schema, model } from 'mongoose';
import bcrypt from 'bcrypt-nodejs';

const userSchema = new Schema({
        email: {
            type: String,
            required: true,
            unique: true,
            lowercase: true
        },
        password: {
            type: String,
            required: true,
        },
        username: {
            type: String,
            required: true,
        },
        age:{
            type:Number,
            required:true
        },
        gender:{
            type:String,
            required:true
        },
        media: {
            type: Object,
            required: false
        },
        token:{
            type:String,
            required:false
        },
        address: {
            type: [String],
            required: false
        }
    },
    {
        timestamps: true
    });

userSchema.pre('save', async function save(next) {
    try {
        if (!this.isModified('password')) {
            return next();
        }
        this.password = bcrypt.hashSync(this.password);
        return next();
    } catch (error) {
        return next(error);
    }
});

userSchema.method({
    transform() {
        const transformed = {};
        const fields = ['id', 'username', 'email'];
        fields.forEach((field) => {
            transformed[field] = this[field];
        });
        return transformed;
    },
    passwordMatches(password) {
        return bcrypt.compareSync(password, this.password);
    }
});

export default model('User', userSchema);
