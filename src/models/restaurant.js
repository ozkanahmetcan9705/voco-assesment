import { Schema, model } from 'mongoose';

const menuSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    image: {
        type: String, // Assuming the image is stored as a URL or path
        required: false
    }
});

const branchSchema = new Schema({
    address: {
        type: String,
        required: true
    },
    city: {
        type: String,
        required: true
    },
    district: {
        type: String,
        required: true
    },
    street: {
        type: String,
        required: true
    },
    location: {
        type: {
            type: String,
            enum: ['Point'],
            default: 'Point'
        },
        coordinates: {
            type: [Number],
            required: true
        }
    },
    menu: [menuSchema],
    owner: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'user'
    }
});

const restaurantSchema = new Schema({
        name: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        logo: {
            type: Object,
            required: false
        },
        branches: [branchSchema],
        restaurantTypes: {
            type: [String],
            required: true
        },
        owner: {
            type: Schema.Types.ObjectId,
            required: true,
            ref: 'user'
        },
        comments: [
            {
                user: {
                    type: Schema.Types.ObjectId,
                    ref: 'User',
                    required: true,
                },
                text: {
                    type: String,
                    required: true,
                },
                point: {
                    type: Number,
                    required: true,
                    min: 1,
                    max: 5,
                },
            },
        ],
    },
    {
        timestamps: true
    });

export default model('Restaurant', restaurantSchema);
