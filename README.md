# Voco Assessment

Bu proje, Voco'nun test görevini içeren bir Node.js uygulamasıdır.

## Kurulum

1. Projeyi klonlayın:

    ```bash
    git clone https://gitlab.com/ozkanahmetcan9705/voco-assesment.git
    ```

2. Proje dizinine gidin:

    ```bash
    cd voco-assesment
    ```

3. Bağımlılıkları yükleyin:

    ```bash
    npm install
    ```

## MongoDB Yapısı

### User Model

Bu şema, kullanıcı bilgilerini depolayan bir MongoDB koleksiyonunu temsil eder. Kullanıcıların e-posta, şifre, kullanıcı adı, yaş, cinsiyet gibi temel bilgileri bulunur. Ayrıca medya dosyaları, tokenlar ve adres bilgileri gibi opsiyonel alanları içerir. Şifre kaydedilmeden önce bcrypt kullanılarak hashlenir ve şemada kullanıcıya özgü özel metodlar (transform ve passwordMatches) tanımlanmıştır.

### Restaurant Model

Bu şema, restoranların ve onların şubelerinin bilgilerini depolayan bir MongoDB koleksiyonunu temsil eder. Restoran adı, açıklaması, logosu, türleri gibi temel bilgileri içerir. Her şube, adres bilgileri, menüsü ve sahibinin kimliği ile ilişkilendirilmiştir. Restoranın yorumları, kullanıcı kimliği, metni ve puanı içeren bir dizi içerir.

### Order Model

Bu şema, kullanıcıların restoranlardan siparişlerini ve bu siparişlere ilişkin bilgileri depolayan bir MongoDB koleksiyonunu temsil eder. Her sipariş, kullanıcı kimliği, restoran kimliği, sipariş öğeleri, toplam fiyat, durum ve yorumlar gibi özelliklere sahiptir. Sipariş öğeleri, menü öğelerinin bilgilerini (isim, fiyat, içerik, görsel vb.) ve miktarını içeren bir dizi içerir. Ayrıca, yorumlar, kullanıcı kimliği, metin ve puan gibi bilgiler içerir.

Her üç modelde de `timestamps: true` seçeneği kullanılarak her belgeye oluşturulma ve güncelleme zaman damgaları eklenmiştir, böylece belgelerin oluşturulma ve güncelleme tarihleri takip edilebilir. Bu şemalar, MongoDB veritabanındaki verilerin düzenli ve yapılandırılmış bir şekilde saklanmasını sağlar.

## EndPointler

### User Endpointleri

`POST /register`

Bu endpoint, yeni kullanıcıların kaydını oluşturmak için kullanılır. Kullanıcıdan alınan e-posta, şifre, kullanıcı adı, cinsiyet ve yaş gibi bilgileri kullanarak yeni bir kullanıcı oluşturur. Ayrıca e-posta adresinin başka bir hesap tarafından kullanılıp kullanılmadığını kontrol eder.

 ```bash
    {
    "email": "kullanici@ornek.com",
    "password": "gucluSifre123",
    "username": "ornek_kullanici",
    "gender": "Male",
    "age": 25
   }
 ```

```bash
Response:
    {
    "status": 200,
    "message": "Hesap oluşturma işlemi başarılı",
    "user": {
        "_id": "user_id",
        "email": "kullanici@ornek.com",
        "username": "ornek_kullanici",
        "gender": "Male",
        "age": 25,
        "token": "jwt_token"
    }
}
 ```
Eğer e-posta başka bir hesap tarafından kullanılıyorsa:
```bash
Response:
    {
    "status": 409,
    "message": "Mail başka bir hesapta kullanılıyor"
}
 ```
Şifre 6 karakterden kısa ise:
```bash
Response:
    {
    "status": 406,
    "message": "Şifre 6 karakter uzunluğunda olmalıdır!"
}
 ```
Beklenmeyen bir hata durumunda:
```bash
Response:
    {
    "status": 500,
    "message": "Beklenmedik bir hata oluştu, HATA_MESAJI"
}
 ```
`GET /login`

Bu endpoint, kullanıcıların giriş yapması için kullanılır. E-posta ve şifre bilgileri ile gelen kullanıcının varlığını ve şifresinin doğruluğunu kontrol eder. Başarılı giriş durumunda kullanıcıya bir JWT (JSON Web Token) sağlar.
```bash
Request:
    {
    "email": "kullanici@ornek.com",
    "password": "gucluSifre123"
}
 ```
```bash
Response:
    {
    "status": 200,
    "message": "OK",
    "token": "jwt_token",
    "user": {
        "_id": "user_id",
        "email": "kullanici@ornek.com",
        "username": "ornek_kullanici",
        "gender": "Male",
        "age": 25
    }
}
 ```
Kullanıcı adı ve şifre uyuşmazsa:
```bash
Response:
    {
    "status": 401,
    "message": "Kullanıcı adı ve şifre uyuşmuyor!"
}
 ```
Beklenmeyen bir hata durumunda:
```bash
Response:
    {
    "status": 500,
    "message": "Beklenmedik bir hata oluştu, HATA_MESAJI"
}
 ```

`PUT /addAddress`

Bu endpoint, kullanıcının adres bilgilerini eklemek için kullanılır. Kullanıcıya ait ID ile verilen adres bilgisi, kullanıcının adres dizisine eklenir.
```bash
Request:
    {
    "id": "user_id",
    "address": "Kullanıcının Adres Bilgisi"
}
 ```
```bash
Response:
    {
    "_id": "user_id",
    "email": "kullanici@ornek.com",
    "username": "ornek_kullanici",
    "gender": "Male",
    "age": 25,
    "address": [
        "Mevcut Adres 1",
        "Mevcut Adres 2",
        "Kullanıcının Adres Bilgisi"
    ]
}
 ```

### Restaurant Endpointleri

`POST /addRestaurant`

Bu endpoint, yeni bir restoran oluşturmak için kullanılır. Kullanıcıdan alınan restoran bilgileri ile yeni bir restoran oluşturur.

```bash
Request:
    {
    "name": "Örnek Restoran",
    "description": "Lezzetli yemekler sunan bir restoran",
    "logo": {"key": "value"},
    "branches": [
        {
            "address": "Örnek Sokak Adı",
            "city": "Örnek Şehir Adı",
            "district": "Örnek Bölge/İlçe",
            "street": "Örnek Mahalle",
            "location": {
                "type": "Point",
                "coordinates": [0, 0]
            },
            "menu": [
                {
                    "name": "Menü 1",
                    "price": 10,
                    "content": "Burger",
                    "image": "menu1.jpg"
                },
                {
                    "name": "Menü 2",
                    "price": 15,
                    "content": "Bir Başka Burger",
                    "image": "menu2.jpg"
                }
            ],
            "owner": "şube sahibinin id'si"
        }
    ],
    "restaurantTypes": ["Örnek Restoran Tipi 1", "Örnek Restoran Tipi 2"],
    "owner": "restoran sahibinin id'si"
}
 ```
```bash
Response:
    {
    "status": 201,
    "message": "Restoran başarıyla oluşturuldu",
    "restaurant": {
        "_id": "restaurant_id",
        "name": "Örnek Restoran",
        "owner": "user_id",
        "description": "Lezzetli yemekler sunan bir restoran",
        "restaurantTypes": ["Fast Food", "Deniz Ürünleri"],
        "branches": [
            {
                "_id": "branch_id",
                "location": {
                    "type": "Point",
                    "coordinates": [41.0082, 28.9784]
                },
                "menu": [
                    { "name": "Hamburger", "price": 15.99 },
                    { "name": "Deniz Mahsulleri Tabagi", "price": 25.99 }
                ]
            }
        ]
    }
}
 ```

`GET /getAllRestaurants`
```bash
Response:
    {
    "status": 200,
    "restaurants": [
        {
            "_id": "restaurant_id",
            "name": "Örnek Restoran",
            "owner": "user_id",
            "description": "Lezzetli yemekler sunan bir restoran",
            "restaurantTypes": ["Fast Food", "Deniz Ürünleri"],
            "branches": [
                {
                    "_id": "branch_id",
                    "location": {
                        "type": "Point",
                        "coordinates": [41.0082, 28.9784]
                    },
                    "menu": [
                        { "name": "Hamburger", "price": 15.99 },
                        { "name": "Deniz Mahsulleri Tabagi", "price": 25.99 }
                    ]
                }
            ]
        },
        // Diğer restoranlar...
    ]
}
 ```
`PUT /addBranch`

Bu endpoint, belirli bir restorana yeni bir şube eklemek için kullanılır. İlgili restoranın sahibi tarafından çağrılmalıdır.
```bash
Request:
    {
    "restaurantId": "restaurant_id",
    "userId": "owner_user_id",
    "location": {
        "type": "Point",
        "coordinates": [41.0151, 28.9792]
    },
    "menu": [
        { "name": "Pide", "price": 12.99 },
        { "name": "Kuzu Tandır", "price": 28.99 }
    ]
}
 ```
```bash
Response:
    {
    "status": 201,
    "message": "Şube başarıyla oluşturuldu",
    "restaurant": {
        "_id": "restaurant_id",
        "name": "Örnek Restoran",
        "owner": "user_id",
        "branches": [
            {
                "_id": "branch_id",
                "location": {
                    "type": "Point",
                    "coordinates": [41.0151, 28.9792]
                },
                "menu": [
                    { "name": "Pide", "price": 12.99 },
                    { "name": "Kuzu Tandır", "price": 28.99 }
                ]
            },
            // Diğer şubeler...
        ]
    }
}
 ```
`PUT /addBranch`

Bu endpoint, belirli bir şubeye menü eklemek için kullanılır. İlgili restoranın sahibi tarafından veya şube sahibi tarafından çağrılmalıdır.

```bash
Request:
    {
    "restaurantId": "restaurant_id",
    "branchId": "branch_id",
    "userId": "user_id",
    "menu": [
        { "name": "Salata", "price": 8.99 },
        { "name": "Tiramisu", "price": 14.99 }
    ]
}
 ```

```bash
Response:
    {
    "status": 201,
    "message": "Menü başarıyla güncellendi",
    "restaurant": {
        "_id": "restaurant_id",
        "branches": [
            {
                "_id": "branch_id",
                "menu": [
                    { "name": "Pide", "price": 12.99 },
                    { "name": "Kuzu Tandır", "price": 28.99 },
                    { "name": "Salata", "price": 8.99 },
                    { "name": "Tiramisu", "price": 14.99 }
                ]
            },
            // Diğer şubeler...
        ]
    }
}
 ```
`PATCH /getMaleComments`

Bu endpoint, bir restorandaki erkek kullanıcıları yaşlarına göre sıralayarak pagine eder. Restoranın ID'si ve sayfa bilgisi ile çalışır.

```bash
Request:
    {
    "restaurantId": "restaurant_id",
    "page": 2,
    "sortOrder": "desc"
}
 ```
```bash
Response:
    {
    "status": 200,
    "maleUsers": [
        {
            "_id": "user_id",
            "email": "kullanici1@ornek.com",
            "username": "kullanici_1",
            "gender": "Male",
            "age": 30
        },
        {
            "_id": "user_id",
            "email": "kullanici2@ornek.com",
            "username": "kullanici_2",
            "gender": "Male",
            "age": 28
        },
        // Diğer erkek kullanıcılar...
    ]
}
 ```
`PUT /addRestaurantType`

Bu endpoint, bir restorana yeni bir restoran tipi eklemek için kullanılır.

```bash
Request:
    {
    "restaurantId": "restaurant_id",
    "restaurantType": "Organik"
}
 ```
```bash
Response:
    {
    "status": 200,
    "message": "Restoran tipi başarıyla eklendi",
    "restaurant": {
        "_id": "restaurant_id",
        "name": "Örnek Restoran",
        "restaurantTypes": ["Fast Food", "Deniz Ürünleri", "Organik"],
        // Diğer restoran bilgileri...
    }
}
 ```


`DELETE /removeRestaurantType`

Bu endpoint, bir restoranın restoran tipini kaldırmak için kullanılır.
```bash
Request:
    {
    "restaurantId": "restaurant_id",
    "restaurantType": "Fast Food"
}
 ```
```bash
Response:
    {
    "status": 200,
    "message": "Restoran tipi başarıyla kaldırıldı",
    "restaurant": {
        "_id": "restaurant_id",
        "name": "Örnek Restoran",
        "restaurantTypes": ["Deniz Ürünleri", "Organik"],
        // Diğer restoran bilgileri...
    }
}
 ```

`GET /filter`

Bu endpoint, belirli kriterlere göre restoranları filtrelemek için kullanılır.
```bash
Response:
    {
    "status": 200,
    "filteredRestaurants": [
        {
            "name": "Örnek Restoran",
            "restaurantTypes": ["Fast Food", "Deniz Ürünleri"],
            "description": "Lezzetli yemekler sunan bir restoran"
        },
        // Diğer restoranlar...
    ]
}
 ```

`GET /getRestaurantsByPoint`

Bu endpoint, restoranları puanlarına göre sıralayarak pagine eder. Sayfa bilgisi ve sayfa boyutu ile çalışır.
```bash
Response:
    {
    "status": 200,
    "pagedRestaurants": [
        {
            "_id": "restaurant_id_1",
            "name": "Restoran 1",
            "averageRating": 4.5
        },
        {
            "_id": "restaurant_id_2",
            "name": "Restoran 2",
            "averageRating": 4.2
        },
        // Diğer restoranlar...
    ]
}
 ```

`GET /nearbyRestaurants`

Bu endpoint, belirli koordinatlara en yakın restoranları getirmek için kullanılır. Koordinatlar bir string olarak gelir ve virgülle ayrılmış enlem ve boylamı içerir.
```bash
Request:
    {
    "coordinates": "41.0082,28.9784"
}
 ```
```bash
Response:
    {
    "status": 200,
    "nearbyRestaurants": [
        {
            "name": "Restoran 1",
            "restaurantTypes": ["Fast Food", "Deniz Ürünleri"],
            "description": "Lezzetli yemekler sunan bir restoran",
            "location": {
                "type": "Point",
                "coordinates": [41.0085, 28.9790]
            }
        },
        {
            "name": "Restoran 2",
            "restaurantTypes": ["İtalyan Mutfağı"],
            "description": "Pizza ve makarna çeşitleriyle ünlü bir İtalyan restoranı",
            "location": {
                "type": "Point",
                "coordinates": [41.0100, 28.9810]
            }
        },
        // Diğer restoranlar...
    ]
}
 ```

### Order Endpointleri

Bu endpoint, kullanıcının bir restoranda sipariş vermesi için kullanılır. Kullanıcı tarafından gönderilen bilgilerle siparişi oluşturur.

`POST /createOrder`

```bash
Request:
    {
    "userId": "user_id",
    "restaurantId": "restaurant_id",
    "items": [
        { "menuItemId": "menu_item_id_1", "quantity": 2 },
        { "menuItemId": "menu_item_id_2", "quantity": 1 }
    ]
}
 ```
```bash
Response:
    {
    "status": 201,
    "message": "Sipariş başarıyla oluşturuldu",
    "order": {
        "_id": "order_id",
        "user": "user_id",
        "restaurant": "restaurant_id",
        "items": [
            { "menuItem": { "name": "Menu Item 1", "price": 10.99, "content": "Açıklama" }, "quantity": 2 },
            { "menuItem": { "name": "Menu Item 2", "price": 15.99, "content": "Açıklama" }, "quantity": 1 }
        ],
        "totalPrice": 37.97,
        "status": "Pending"
    }
}
 ```

`GET /orderDetail/:orderId`

Bu endpoint, bir siparişin detaylarını getirmek için kullanılır. Siparişin ID'si parametre olarak verilir.

```bash
Response:
    {
    "status": 200,
    "orderDetails": {
        "items": [
            { "menuItem": { "name": "Menu Item 1", "price": 10.99, "content": "Açıklama" }, "quantity": 2 },
            { "menuItem": { "name": "Menu Item 2", "price": 15.99, "content": "Açıklama" }, "quantity": 1 }
        ],
        "totalPrice": 37.97,
        "status": "Pending"
    }
}
 ```

`PUT /updateStatus/:orderId`

Bu endpoint, bir siparişin durumunu güncellemek için kullanılır. Siparişin ID'si ve yeni durumu verilir.
```bash
Request:
    {
    "newStatus": "Delivered"
}
 ```
```bash
Response:
    {
    "status": 200,
    "message": "Sipariş durumu başarıyla güncellendi"
}
 ```

`PUT /addCommentAndRating`

Bu endpoint, bir siparişe yorum ve puan eklemek için kullanılır. Siparişin ID'si, kullanıcının ID'si, yorum metni ve puan bilgisi verilir. Yorum ve puan aynı zamanda restoranın yorumlarına ve puanlarına eklenir.
```bash
Request:
    {
    "orderId": "order_id",
    "userId": "user_id",
    "text": "Lezzetliydi!",
    "point": 5
}
 ```
```bash
Response:
    {
    "status": 200,
    "message": "Yorum ve puan başarıyla eklendi",
    "order": {
        "_id": "order_id",
        "user": "user_id",
        "restaurant": "restaurant_id",
        "items": [...],
        "totalPrice": 37.97,
        "status": "Delivered",
        "comments": [
            { "user": "user_id", "text": "Lezzetliydi!", "point": 5 },
            // Diğer yorumlar...
        ]
    }
}
 ```
